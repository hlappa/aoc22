defmodule CalorieCounting do
  def part1 do
    input() |> Enum.map(&Enum.sum/1) |> Enum.max()
  end

  def part2 do
    input() |> Enum.map(&Enum.sum/1) |> Enum.sort() |> Enum.take(-3) |> Enum.sum()
  end

  defp input do
    case File.read("input.txt") do
      {:ok, content} ->
        content
        |> String.split("\n\n", trim: true)
        |> Enum.map(fn s -> String.split(s, "\n") end)
        |> Enum.map(fn n ->
          Enum.filter(n, fn s -> String.length(s) != 0 end) |> Enum.map(&String.to_integer/1)
        end)

      {:err, error} ->
        IO.inspect(error)
    end
  end
end

IO.puts("Part 1:")
CalorieCounting.part1() |> IO.inspect()

IO.puts("Part 2:")
CalorieCounting.part2() |> IO.inspect()
