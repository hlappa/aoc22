defmodule RockPaperScissors do
  @part1 %{
    "A Y": 8,
    "B Y": 5,
    "C Y": 2,
    "A X": 4,
    "B X": 1,
    "C X": 7,
    "A Z": 3,
    "B Z": 9,
    "C Z": 6
  }

  @part2 %{
    "A Y": 4,
    "B Y": 5,
    "C Y": 6,
    "A X": 3,
    "B X": 1,
    "C X": 2,
    "A Z": 8,
    "B Z": 9,
    "C Z": 7
  }

  def part1 do
    input() |> Enum.map(fn sym -> @part1[:"#{sym}"] end) |> Enum.sum()
  end

  def part2 do
    input() |> Enum.map(fn sym -> @part2[:"#{sym}"] end) |> Enum.sum()
  end

  defp input do
    case File.read("input.txt") do
      {:ok, content} ->
        content
        |> String.split("\n", trim: true)

      {:err, error} ->
        IO.inspect(error)
    end
  end
end

IO.puts("Part 1:")
RockPaperScissors.part1() |> IO.inspect()
IO.puts("Part 2:")
RockPaperScissors.part2() |> IO.inspect()
