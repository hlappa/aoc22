defmodule Rucksack do
  @alphabets "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

  def part1 do
    input() |> Enum.map(&value/1) |> Enum.sum()
  end

  def part2 do
    input()
    |> Enum.chunk_every(3)
    |> Enum.map(fn set -> Enum.map(set, &normalize/1) end)
    |> Enum.map(&value2/1)
    |> Enum.sum()
  end

  defp value(str) do
    {head, tail} = head_and_tail(str)
    letter = (head ++ tail) |> get_letter_by_freq(2)

    (:binary.match(@alphabets, letter) |> elem(0)) + 1
  end

  defp value2([head, middle, tail] = set) do
    letter = (head ++ middle ++ tail) |> get_letter_by_freq(3)

    (:binary.match(@alphabets, letter) |> elem(0)) + 1
  end

  defp get_letter_by_freq(alphabets, num) do
    alphabets
    |> Enum.frequencies()
    |> Enum.find(fn {k, val} -> val == num end)
    |> elem(0)
  end

  defp head_and_tail(str) do
    length = String.length(str)

    head = String.slice(str, 0, div(length, 2)) |> normalize()
    tail = String.slice(str, div(length, 2), length) |> normalize()

    {head, tail}
  end

  defp normalize(str) do
    String.split(str, "")
    |> Enum.filter(fn s -> String.length(s) != 0 end)
    |> Enum.uniq()
  end

  defp input do
    case File.read("input.txt") do
      {:ok, content} ->
        content
        |> String.split("\n", trim: true)

      {:err, error} ->
        IO.inspect(error)
    end
  end
end

IO.puts("Part 1:")
Rucksack.part1() |> IO.inspect()

IO.puts("Part 2:")
Rucksack.part2() |> IO.inspect()
