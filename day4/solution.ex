defmodule CampCleanup do
  def part1 do
    input() |> Enum.filter(&contains/1) |> Enum.count()
  end

  def part2 do
    input() |> Enum.filter(&overlaps/1) |> Enum.count()
  end

  defp contains(pair) do
    {head, tail} = parse_pairs(pair)

    Enum.all?(head, fn n -> Enum.member?(tail, n) end) ||
      Enum.all?(tail, fn n -> Enum.member?(head, n) end)
  end

  defp overlaps(pair) do
    {head, tail} = parse_pairs(pair)

    !Range.disjoint?(hd(head)..List.last(head), hd(tail)..List.last(tail))
  end

  defp parse_pairs(pair) do
    [head, tail] =
      String.split(pair, ",")
      |> Enum.map(fn n ->
        String.split(n, "-") |> Enum.map(&String.to_integer/1)
      end)

    hd_elf = Enum.to_list(hd(head)..List.last(head))
    tail_elf = Enum.to_list(hd(tail)..List.last(tail))

    {hd_elf, tail_elf}
  end

  defp input do
    case File.read("input.txt") do
      {:ok, content} ->
        content
        |> String.split("\n", trim: true)

      {:err, error} ->
        IO.inspect(error)
    end
  end
end

IO.puts("Part 1:")
CampCleanup.part1() |> IO.inspect()

IO.puts("Part 2:")
CampCleanup.part2() |> IO.inspect()
