defmodule SupplyStacks do
  def part1 do
    initial_arrs = parse_initial_arrays()
    instructions = parse_instructions()

    get_first_packs(instructions, initial_arrs, false)
  end

  def part2 do
    initial_arrs = parse_initial_arrays()
    instructions = parse_instructions()

    get_first_packs(instructions, initial_arrs, true)
  end

  defp get_first_packs(instructions, init_state, multiple_at_once) do
    Enum.reduce(instructions, init_state, fn [move_count, from, to], acc ->
      array_to_pop = Enum.at(acc, from - 1)

      popped_values =
        if multiple_at_once do
          Enum.take(array_to_pop, -move_count)
        else
          Enum.take(array_to_pop, -move_count) |> Enum.reverse()
        end

      inserted_array = Enum.at(acc, to - 1) ++ popped_values
      popped_array = Enum.slice(array_to_pop, 0, Enum.count(array_to_pop) - move_count)

      List.replace_at(acc, to - 1, inserted_array) |> List.replace_at(from - 1, popped_array)
    end)
    |> Enum.map(&List.last/1)
    |> List.to_string()
  end

  defp parse_initial_arrays do
    Enum.reduce([1, 5, 9, 13, 17, 21, 25, 29, 33], [], fn r, acc ->
      arr =
        Enum.reduce(0..7, [], fn n, acc ->
          letter = Enum.at(input(), n) |> String.at(r)

          acc ++ [letter]
        end)
        |> Enum.reverse()
        |> Enum.reject(fn n -> n == " " or n == nil end)

      List.insert_at(acc, r, arr)
    end)
  end

  defp parse_instructions do
    input()
    |> Enum.slice(9..-1)
    |> Enum.map(fn n ->
      String.split(n, " ", trim: true) |> Enum.drop_every(2) |> Enum.map(&String.to_integer/1)
    end)
  end

  defp input do
    case File.read("input.txt") do
      {:ok, content} ->
        content
        |> String.split("\n", trim: true)

      {:err, error} ->
        IO.inspect(error)
    end
  end
end

IO.puts("Part 1:")
SupplyStacks.part1() |> IO.inspect()
IO.puts("Part 2:")
SupplyStacks.part2() |> IO.inspect()
