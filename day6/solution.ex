defmodule TuningTrouble do
  def part1 do
    str = input()

    get_count(str, 4)
  end

  def part2 do
    str = input()

    get_count(str, 14)
  end

  defp get_count(str, step) do
    Enum.reduce_while(0..String.length(str), 0, fn n, acc ->
      chars = String.slice(str, n..(n + step - 1)) |> String.split("", trim: true) |> Enum.uniq()

      if Enum.count(chars) == step do
        {:halt, n + step}
      else
        {:cont, acc}
      end
    end)
  end

  defp input do
    case File.read("input.txt") do
      {:ok, content} ->
        content |> String.split("\n", trim: true) |> hd()

      {:err, error} ->
        IO.inspect(error)
    end
  end
end

IO.puts("Part 1:")
TuningTrouble.part1() |> IO.inspect()
IO.puts("Part 2:")
TuningTrouble.part2() |> IO.inspect()
