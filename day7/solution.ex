defmodule NoSpace do
  def part1 do
    input() |> run_terminal(false)
  end

  def part2 do
    input() |> run_terminal(true)
  end

  defp run_terminal(cmd_history, delete) do
    results =
      Enum.reduce(cmd_history, %{path: [], dirs: []}, fn cmd, acc ->
        parts = String.split(cmd, " ", trim: true)

        case line_type(cmd) do
          :cmd ->
            case {Enum.at(parts, 1), Enum.at(parts, 2)} do
              {"cd", ".."} ->
                new_path = Map.get(acc, :path) |> List.pop_at(-1) |> elem(1)
                %{acc | path: new_path}

              {"cd", dir} ->
                if dir == "/" do
                  %{acc | path: ["/"]}
                else
                  path = Map.get(acc, :path) |> List.last()

                  %{acc | path: Map.get(acc, :path) ++ ["#{if path != "/", do: path}/#{dir}"]}
                end

              {"ls", _} ->
                acc
            end

          :dir ->
            acc

          :file ->
            size = Enum.at(parts, 0) |> String.to_integer()
            paths = Map.get(acc, :path)
            dirs = Map.get(acc, :dirs)

            new_dirs =
              Enum.reduce(paths, dirs, fn n, acc ->
                prev = acc[:"#{n}"]

                if prev == nil do
                  Keyword.put(acc, :"#{n}", size)
                else
                  Keyword.replace(acc, :"#{n}", size + prev)
                end
              end)

            %{acc | dirs: new_dirs}
        end
      end)
      |> Map.get(:dirs)
      |> Keyword.values()
      |> Enum.sort()

    if delete do
      used = Enum.max(results)

      Enum.reduce(results, [], fn val, acc ->
        if 70_000_000 - used + val >= 30_000_000 do
          acc ++ [val]
        else
          acc
        end
      end)
      |> Enum.min()
    else
      results
      |> Enum.filter(fn n -> n < 100_000 end)
      |> Enum.sum()
    end
  end

  defp line_type("$" <> _rest), do: :cmd
  defp line_type("dir" <> _rest), do: :dir
  defp line_type(_), do: :file

  defp input do
    case File.read("input.txt") do
      {:ok, content} ->
        content |> String.split("\n", trim: true)

      {:err, error} ->
        IO.inspect(error)
    end
  end
end

IO.puts("Part 1:")
NoSpace.part1() |> IO.inspect()

IO.puts("Part 2:")
NoSpace.part2() |> IO.inspect()
